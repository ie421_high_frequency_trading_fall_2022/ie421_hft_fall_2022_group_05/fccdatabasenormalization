CREATE TABLE dbo.PUBACC_LA
(
      record_type               char(2)              null,
      unique_system_identifier  numeric(9,0)         null,
      callsign                  char(10)             null,
      attachment_code           char(1)              null,
      attachment_desc           varchar(60)          null,
      attachment_date           char(10)             null,
      attachment_filename       varchar(60)          null,
      action_performed          char(1)              null
)
