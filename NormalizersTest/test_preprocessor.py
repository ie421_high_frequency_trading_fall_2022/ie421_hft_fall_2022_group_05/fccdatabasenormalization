import unittest

from preprocessor import Preprocessor


class test_preprocessor(unittest.TestCase):

    def setUp(self):
        self.testSQLSchema = "../NormalizersTest/testData/testSQLSchema.txt"
        self.testJankSchema = "../NormalizersTest/testData/poorlyFormatted.txt"
        self.testUnjankSchema = "../NormalizersTest/testData/properlyFormatted.txt"
        self.testMultiTable = "../NormalizersTest/testData/testMultiTable.txt"

    def test_SQLSchema(self):
        file = open(self.testSQLSchema, 'r')
        sql_table = file.read()
        table_obj = Preprocessor.get_table(sql_table)

        self.assertEqual("dbo.PUBACC_AH", table_obj.name)
        expectedDict = {"record_type": "char(2)", "unique_system_identifier": "numeric(9,0)",
                        "uls_file_num": "char(14)", "attachment_desc": "varchar(60)", "attachment_file_id": "char(18)"}
        self.assertEqual(expectedDict, table_obj.cols)

    def test_fix_formatting(self):
        file = open(self.testJankSchema, 'r')
        sql_table = Preprocessor.fix_formatting(file.read())
        proper_file = open(self.testUnjankSchema, 'r')
        proper_table = proper_file.read()
        self.assertEqual(sql_table, proper_table)

    def test_splitTables(self):
        file = open(self.testMultiTable, "r")
        tables = Preprocessor.split_tables(file.read())
        self.assertEqual(len(tables), 2)

        first_table = open(self.testUnjankSchema, "r").read()
        second_table = open(self.testSQLSchema, "r").read()

        self.assertEqual(tables[0].strip(), first_table.strip())
        self.assertEqual(tables[1].strip(), second_table.strip())

