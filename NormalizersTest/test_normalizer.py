import unittest

from Normalizers.Normalizer import Normalizer


class NormalizerTest(unittest.TestCase):

    def setUp(self):
        self.Normalizer = Normalizer("3NF")
        self.testFDsPath = "../NormalizersTest/testData/testFDs.txt"
        self.testRelationPath = "../NormalizersTest/testData/testRelation.txt"

    def test_ReadRelation(self):
        self.Normalizer.readRelation(self.testRelationPath)
        self.assertEqual(len(self.Normalizer.attributes), 6)

        expectedSet = {"A", "B", "C", "D", "E", "F"}
        self.assertEqual(expectedSet, self.Normalizer.attributes)

    def test_ReadFDs(self):
        self.Normalizer.readFD(self.testFDsPath)
        self.assertEqual(len(self.Normalizer.fds), 5)

        expectedDict = {"B": "C", "D": "C,E", "D,F": "B", "C": "E,F", "B,F": "C,E"}
        for key, value in self.Normalizer.fds.items():
            self.assertTrue(key in expectedDict)
            self.assertEqual(expectedDict[key], value)


if __name__ == '__main__':
    unittest.main()
