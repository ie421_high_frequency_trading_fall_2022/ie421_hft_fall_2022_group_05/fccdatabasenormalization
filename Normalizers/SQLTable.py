class SQLTable:

    def __init__(self, name):
        self.name = name
        self.cols = dict()

    def add_column(self, column_name, data_type):
        self.cols[column_name] = data_type
