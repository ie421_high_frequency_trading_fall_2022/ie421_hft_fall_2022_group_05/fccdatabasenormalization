class Normalizer:

    def __init__(self, mode='3NF'):
        self.type = mode
        self.attributes = set()
        self.fds = dict()

    def readRelation(self, relation_path):
        relations = open(relation_path, "r")

        for attribute in relations:
            self.attributes.add(attribute.rstrip()[0])

        relations.close()

    def readFD(self, fd_path):
        fds = open(fd_path, "r")

        for fd in fds:
            fd_tuple = tuple(fd.split("->"))
            self.fds[fd_tuple[0]] = fd_tuple[1].rsplit()[0]

        fds.close()

    def normalize(self, relation_path, fd_path):
        self.readRelation(relation_path)
        self.readFD(fd_path)


