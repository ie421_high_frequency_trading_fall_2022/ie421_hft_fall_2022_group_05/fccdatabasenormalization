import sqlparse
from SQLTable import SQLTable


class Preprocessor:

    @staticmethod
    def split_tables(sql_text):
        tables = sql_text.split("go")
        return tables[:-1]  # remove last element since it is a blank line

    @staticmethod
    def fix_formatting(sql_text):  # inconsistent null, vs null , and turn all create table to uppercase
        return sql_text.replace("create table", "CREATE TABLE").replace("Null", "null").replace("null ,", "null,")

    @staticmethod
    def get_table_name(tokens):
        for token in reversed(tokens):
            if token.ttype is None:
                return token.value
        return " "

    # From https://stackoverflow.com/questions/63247330/python-sql-parser-to-get-the-column-name-and-data-type
    @staticmethod
    def get_table(sql_string):
        table = None
        parse = sqlparse.parse(sql_string)
        for stmt in parse:
            # Get all the tokens except whitespaces
            tokens = [t for t in sqlparse.sql.TokenList(stmt.tokens) if t.ttype != sqlparse.tokens.Whitespace]
            is_create_stmt = False
            for i, token in enumerate(tokens):
                # Is it a create statements ?
                if token.match(sqlparse.tokens.DDL, 'CREATE'):
                    is_create_stmt = True
                    continue

                # If it was a create statement and the current token starts with "("
                if is_create_stmt and token.value.startswith("("):

                    # Get the table name by looking at the tokens in reverse order till you find
                    # a token with None type
                    table = SQLTable(Preprocessor.get_table_name(tokens[:i]))

                    # Now parse the columns
                    txt = token.value
                    columns = txt[1:txt.rfind(")")].replace("\n", "").split("null,")
                    for column in columns:
                        c = ' '.join(column.split()).split()
                        c_name = c[0].replace('\"', "")
                        c_type = c[1]  # For condensed type information
                        # OR
                        # c_type = " ".join(c[1:]) # For detailed type information

                        table.add_column(c_name, c_type)
        if table is None:
            raise Exception("Could not parse SQL Create statement")
        else:
            return table
